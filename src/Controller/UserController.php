<?php


namespace App\Controller;



use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 * @package App\Controller
 * @Route("/user")
 */
class UserController extends Controller
{
    /**
     * @Route("/profile", name="user_profile")
     */
    public function userProfileAction()
    {
        return $this->render('user/profile.html.twig');
    }
}